# Nek5000-Statistics-1D-Profile-Base-Case

This is a base case for channel flow with averaging in the homogeneous directions.
NOTE: At this point the mean velocity is computed at every time step and not averaged over one eddy turn over time.